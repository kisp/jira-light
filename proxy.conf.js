
if(!process.env.JIRA_URL) {
  console.error("JIRA_URL env is not specified!");
  process.exit(1);
}

const PROXY_CONFIG = {
  "/jira": {
    "target": process.env.JIRA_URL,
    "secure": true,
    "changeOrigin": true,
    "logLevel": "debug",
    "pathRewrite": {
      "^/jira": ""
    },
    "credentials": "include",
  }
};

module.exports = PROXY_CONFIG;
