import {Injectable} from '@angular/core';
import {NavigationEnd, Router, Routes} from '@angular/router';
import {AuthCredentialsComponent} from '../components/auth-credentials/auth-credentials.component';
import {TicketViewComponent} from '../components/ticket-view/ticket-view.component';
import {FiltersViewComponent} from '../components/filters-view/filters-view.component';
import {Observable} from 'rxjs';
import {filter, map} from 'rxjs/operators';
import {DashboardsViewComponent} from '../components/dashboards-view/dashboards-view.component';
import {ProjectsViewComponent} from '../components/projects-view/projects-view.component';
import {DashboardViewComponent} from '../components/dashboard-view/dashboard-view.component';
import {MenuItem} from 'primeng/api';

const loadBoardModule = () => import('../components/board/board.module').then(m => m.BoardModule);
const loadDependencyViewModule = () => import('../components/dependency-view/dependency-view.module').then(m => m.DependencyViewModule);
@Injectable({
  providedIn: 'root'
})
export class RoutingService {
  static ROUTES: Routes = [
    {path: 'auth', component: AuthCredentialsComponent},
    {path: 'ticket', component: TicketViewComponent},
    {path: 'filters', component: FiltersViewComponent},
    {path: 'dashboards', component: DashboardsViewComponent},
    {path: 'dashboard', component: DashboardViewComponent},
    {
      path: 'board',
      loadChildren: loadBoardModule
    },
    {
      path: 'dependencies',
      loadChildren: loadDependencyViewModule
    },
    {path: 'projects', component: ProjectsViewComponent},
    {
      path: '',
      redirectTo: '',
      pathMatch: 'full'
    }
  ];

  menuItems: MenuItem[] = [
    {label: 'Config', icon: 'pi pi-fw pi-sitemap', routerLink: '/auth'},
    {label: 'Board', icon: 'pi pi-fw pi-desktop', routerLink: '/board'},
    {label: 'Find ticket', icon: 'pi pi-fw pi-file', routerLink: '/ticket'},
    {
      label: 'Experimental', items: [
        {label: 'Dependency analysis', icon: 'pi pi-fw pi-desktop', routerLink: '/dependencies'},
        {label: 'Projects', routerLink: '/projects'},
        {label: 'Dashboards', routerLink: '/dashboards'},
        {label: 'Dashboard', routerLink: '/dashboard'}
      ]
    },
    {
      label: 'TODO', items: [
        {label: 'Filters', routerLink: '/filters'},
      ]
    }
  ];

  constructor(private router: Router) {
  }

  getMenuItemsWithSelection(): Observable<MenuItem[]> {
    return this.router.events.pipe(
      filter(e => e instanceof NavigationEnd),
      map(e => this.menuItems.map(menuItem => ({
          ...menuItem,
          styleClass: menuItem.routerLink === (e as NavigationEnd).url ? 'activeMenuItem' : ''
        }))
      ));
  }
}
