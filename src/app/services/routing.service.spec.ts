import {async, TestBed} from '@angular/core/testing';

import {RoutingService} from './routing.service';
import {NavigationEnd, Router} from '@angular/router';
import {Subject} from 'rxjs';
import {RouterTestingModule} from '@angular/router/testing';
import {MenuItem} from 'primeng/api';

describe('RoutingService', () => {

  let routerEvents: Subject<NavigationEnd>;

  beforeEach(async(() => {
    routerEvents = new Subject<NavigationEnd>();
    TestBed.configureTestingModule({
      declarations: [],
      imports: [],
      providers: [
        RoutingService,
        {
          provide: Router,
          useValue: {
            events: routerEvents.asObservable()
          }
        }
      ]
    });
  }));

  it('should be created', () => {
    const service: RoutingService = TestBed.get(RoutingService);
    expect(service).toBeTruthy();
  });

  it('should have the ROUTES declarations', () => {
    expect(RoutingService.ROUTES).toBeTruthy();
  });

  it('should return a route with activeMenuItem style', (done) => {
    // GIVEN
    const service: RoutingService = TestBed.get(RoutingService);

    // WHEN
    service.getMenuItemsWithSelection().subscribe((menu: MenuItem[]) => {
      // THEN
      expect(menu[2].routerLink).toBe('/ticket');
      expect(menu[2].styleClass).toBe('activeMenuItem');

      done();
    });

    routerEvents.next( new NavigationEnd( 1, '/ticket', '/ticket'));
  });
});
