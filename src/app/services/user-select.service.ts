import {Injectable} from '@angular/core';
import {Observable, ReplaySubject, Subject} from 'rxjs';
import {EntityLocalStorageService} from '../data-access-layer/entity-local-storage/entity-local-storage.service';

@Injectable({
  providedIn: 'root'
})
export class UserSelectService {
  private subscribers: Map<string, Subject<string>>;

  constructor(private database: EntityLocalStorageService) {
    this.subscribers = new Map<string, Subject<string>>();
  }

  userSet(key: string, value: string, storeValue = false) {
    if (storeValue) {
      this.storeAndPropagate(key, value);
    } else {
      this.propagate(key, value);
    }
  }

  userSelected(key: string, propagateFirst = true): Observable<string> {
    if (!this.subscribers.has(key)) {
      const subject: Subject<string> = new ReplaySubject<string>();
      this.subscribers.set(key, subject);

      if (propagateFirst) {
        this.loadAndPropagate(key, subject);
      }

      return subject.asObservable();
    } else {
      return this.subscribers.get(key).asObservable();
    }
  }

  fetch(key: string): Promise<string> {
    return this.database.load(key);
  }

  fetchMultiple(key: string): Promise<string[]> {
    return this.database.fetchMultiple(key);
  }

  private storeAndPropagate(key: string, value: string) {
    this.database.save(key, value).then(
      () => this.propagate(key, value));
  }

  private propagate(key: string, value: string) {
    if (!this.subscribers.has(key)) {
      this.subscribers.set(key, new Subject<string>());
    }
    this.subscribers.get(key).next(value);
  }

  private loadAndPropagate(key: string, subject: Subject<string>) {
    this.database.load(key).then(firstValue =>
      subject.next(firstValue));
  }
}
