import {TestBed} from '@angular/core/testing';

import {UserSelectService} from './user-select.service';
import {MessageService} from 'primeng/api';
import {EntityLocalStorageService} from '../data-access-layer/entity-local-storage/entity-local-storage.service';

describe('UserSelectService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [],
    providers: [
      {
        provide: MessageService,
        useValue: {}
      },
      {
        provide: EntityLocalStorageService,
        useValue: {}
      }
    ]
  }));

  it('should be created', () => {
    const service: UserSelectService = TestBed.get(UserSelectService);
    expect(service).toBeTruthy();
  });
});
