import {async, TestBed} from '@angular/core/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {AppComponent} from './app.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {RoutingService} from './services/routing.service';
import {of} from 'rxjs';

describe('AppComponent', () => {

  let menuServiceSpy;

  beforeEach(async(() => {
    menuServiceSpy = jasmine.createSpy('menuService').and.returnValue(of([]));
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
      ],
      declarations: [
        AppComponent
      ],
      providers: [
        {
          provide: RoutingService,
          useValue: {
            getMenuItemsWithSelection: menuServiceSpy
          }
        }
      ],
      schemas: [ NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it('should call the menu service', () => {

    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    fixture.detectChanges();
    expect(menuServiceSpy).toHaveBeenCalled();
  });
});
