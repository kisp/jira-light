import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {RoutingService} from './services/routing.service';

@NgModule({
  imports: [RouterModule.forRoot(RoutingService.ROUTES,
    {
      enableTracing: false
    })],
  exports: [RouterModule]
})
export class AppRoutingModule { }

