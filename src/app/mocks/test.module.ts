import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {CardComponent, TooltipDirective} from './primeng';

@NgModule({
  declarations: [TooltipDirective,
    CardComponent],
  imports: [
    CommonModule
  ],
  exports: [
  ]
})
export class TestModule { }
