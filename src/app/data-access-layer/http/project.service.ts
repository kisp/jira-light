import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {ServiceConfigurationService} from './service-configuration.service';
import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs/operators';
import {Project} from '../../models/Project';
import {combinePaginateWithNextPageLink} from './utils';


@Injectable({
  providedIn: 'root'
})
export class ProjectService extends ServiceConfigurationService {
  constructor(private http: HttpClient) {
    super();
  }

  getProjects(): Observable<Project[]> {

    const request = (url: string) => this.http.get(`${this.server}${url}`, this.httpHeaders);

    return combinePaginateWithNextPageLink(request, '/rest/api/3/project/search')
      .pipe(
        map(a => a.map(response => response.values).reduce((acc, c) => acc.concat(c))),
        map(a => a.map(p => new Project(p)))
      );
  }
}
