import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {ServiceConfigurationService} from './service-configuration.service';

@Injectable({
  providedIn: 'root'
})
export class FilterService extends ServiceConfigurationService {
  constructor(private http: HttpClient) {
    super();
  }

  searchFilters(accountId: string): Observable<any> {
    return this.http.get(`${this.server}/rest/api/3/filter/search?accountId=${accountId}`,
      this.httpHeaders);
  }
}
