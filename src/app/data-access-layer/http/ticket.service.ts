import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ServiceConfigurationService } from './service-configuration.service';
import { combinePaginate, seemlessPaginate } from './utils';
import { Ticket } from '../../models/Ticket';
import { filter, map, shareReplay, withLatestFrom } from 'rxjs/operators';
import { CustomField } from '../../models/CustomField';
import { HttpWithMonitorService } from './http-with-monitor.service';
import { Transition } from '../../models/Transition';

@Injectable({
    providedIn: 'root'
})
export class TicketService extends ServiceConfigurationService {

    private customFields$: Observable<CustomField[]>;

    constructor(private http: HttpWithMonitorService) {
        super();

        this.customFields$ = this.getCustomFieldConfigurations().pipe(shareReplay(1));
    }

    getTicket(key: string): Observable<Ticket> {
        return this.http.get(`${this.server}/rest/api/3/issue/${key}`,
            this.httpHeaders)
            .pipe(
                map(o => new Ticket(o)),
                withLatestFrom(this.customFields$),
                map(([ticket, fieldDefinitions]) => {
                        ticket.fieldDefinitions = fieldDefinitions;
                        return ticket;
                    }
                ));
    }

    searchAllSeamless(jql: string): Observable<Ticket[]> {
        const request = (queryParams = {}) => {
            return this.http.get(`${this.server}/rest/api/3/search?maxResults=20`,
                {
                    ...this.httpHeaders,
                    params: {
                        jql,
                        ...queryParams
                    }
                }
            );
        };

        return seemlessPaginate<Ticket>(
            request,
            (acc, curr) => acc.concat(
                (curr.issues || []).map(i => new Ticket(i)))
        ).pipe(
            withLatestFrom(this.customFields$),
            map(([tickets, fieldDefinitions]) => {
                tickets.forEach(t => t.fieldDefinitions = fieldDefinitions);
                return tickets;
            })
        );
    }

    searchAllWaitForCombine(jql: string): Observable<Ticket[]> {
        const request = (queryParams = {}) => {
            return this.http.get(`${this.server}/rest/api/3/search?maxResults=20`,
                {
                    ...this.httpHeaders,
                    params: {
                        jql,
                        ...queryParams
                    }
                }
            );
        };

        return combinePaginate<Ticket, any>(
            request,
            (acc, curr) => acc.concat(
                (curr.issues || []).map(i => new Ticket(i))),
            100
        )
            .pipe(
                withLatestFrom(this.customFields$),
                map(([tickets, fieldDefinitions]) => {
                    tickets.forEach(t => t.fieldDefinitions = fieldDefinitions);
                    return tickets;
                })
            );
    }

    getTransitions(issueIdOrKey: string): Observable<Transition[]> {
        return this.http.get(`${this.server}/rest/api/3/issue/${issueIdOrKey}/transitions`, this.httpHeaders)
            .pipe(
                map(o => o.transitions.map(t => new Transition(t)))
            );
    }

    /**
     * https://developer.atlassian.com/cloud/jira/platform/rest/v3/#api-rest-api-3-issue-issueIdOrKey-transitions-post
     */
    transitionTo(issueIdOrKey: string, transitionId: string): Observable<any> {
        return this.http.post(`${this.server}/rest/api/3/issue/${issueIdOrKey}/transitions`,
            {
                transition: {id: transitionId}
            },
            this.httpHeaders);
    }

    assignTo(issueIdOrKey: string, accountId: string): Observable<any> {
        return this.http.put(`${this.server}/rest/api/3/issue/${issueIdOrKey}/assignee`,
            {
                accountId
            },
            this.httpHeaders);
    }

    private getCustomFieldConfigurations(): Observable<CustomField[]> {
        return this.http.get(`${this.server}/rest/api/3/field`, this.httpHeaders)
            .pipe(
                filter(arr => Array.isArray(arr)),
                map(arr => (arr as Array<CustomField>).map(o => new CustomField(o))));
    }
}
