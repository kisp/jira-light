import {TestBed} from '@angular/core/testing';

import {HttpWithMonitorService} from './http-with-monitor.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {MessageService} from 'primeng/api';
import {HttpClient} from '@angular/common/http';

describe('HttpWithMonitorService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [],
    providers: [
      {
        provide: MessageService,
        useValue: {}
      },
      {
        provide: HttpClient,
        useValue: {}
      }
    ]
  }));

  it('should be created', () => {
    const service: HttpWithMonitorService = TestBed.get(HttpWithMonitorService);
    expect(service).toBeTruthy();
  });
});
