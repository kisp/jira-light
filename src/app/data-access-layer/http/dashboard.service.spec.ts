import { TestBed } from '@angular/core/testing';

import { DashboardService } from './dashboard.service';
import {HttpClient} from '@angular/common/http';

describe('DashboardService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      {
        provide: HttpClient,
        useValue: {
        }
      }
    ]
  }));

  it('should be created', () => {
    const service: DashboardService = TestBed.get(DashboardService);
    expect(service).toBeTruthy();
  });
});
