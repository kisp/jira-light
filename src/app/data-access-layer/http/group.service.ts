import { Injectable } from '@angular/core';
import {ServiceConfigurationService} from './service-configuration.service';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GroupService extends ServiceConfigurationService {
  constructor(private http: HttpClient) {
    super();
  }

  search(accountId: string): Observable<any> {
    return this.http.get(`${this.server}/rest/api/3/user/groups?accountId=${accountId}`,
      this.httpHeaders);
  }
}
