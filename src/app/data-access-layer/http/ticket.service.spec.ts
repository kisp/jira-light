import { TestBed } from '@angular/core/testing';

import { TicketService } from './ticket.service';
import {HttpClient} from '@angular/common/http';
import {HttpWithMonitorService} from './http-with-monitor.service';
import {NEVER} from 'rxjs';

describe('TicketService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      {
        provide: HttpWithMonitorService,
        useValue: {
          get: () => NEVER
        }
      }
    ]
  }));

  it('should be created', () => {
    const service: TicketService = TestBed.get(TicketService);
    expect(service).toBeTruthy();
  });
});
