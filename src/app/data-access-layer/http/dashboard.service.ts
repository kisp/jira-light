import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {forkJoin, Observable} from 'rxjs';
import {ServiceConfigurationService} from './service-configuration.service';
import {combinePaginateWithNextPageLink} from './utils';
import {map, mergeMap} from 'rxjs/operators';
import {Dashboard} from '../../models/Dashboard';

@Injectable({
  providedIn: 'root'
})
export class DashboardService extends ServiceConfigurationService {
  constructor(private http: HttpClient) {
    super();
  }

  //
  // getMyDashboards(): Observable<any> {
  //   return this.http.get(`${this.server}/rest/api/3/dashboard?filter=my`,
  //     {
  //       headers: {
  //         Authorization: this.authHeader,
  //         Accept: 'application/json',
  //         'Content-Type': 'application/json'
  //       }
  //     });
  // }

  searchDashboardsByProjectId(projectId: number): Observable<any> {
    const request = (url: string) => this.http.get(`${this.server}${url}`, this.httpHeaders);

    return combinePaginateWithNextPageLink(
      request, `/rest/api/3/dashboard/search?projectId=${projectId}`)
      .pipe(
        map(a => a.map(response => response.values).reduce((acc, c) => acc.concat(c))),
        map(a => a.map(p => new Dashboard(p)))
      );
  }

  searchDashboardsByGroupname(groupname: string): Observable<Array<Dashboard>> {
    const request = (url: string) => this.http.get(`${this.server}${url}`, this.httpHeaders);

    return combinePaginateWithNextPageLink(
      request,
      `/rest/api/3/dashboard/search?groupname=${groupname}`)
      .pipe(
        map(a => a.map(response => response.values).reduce((acc, c) => acc.concat(c))),
        mergeMap(projectIds =>
          forkJoin(...projectIds.map(p => this.getDashboard(p.id)))),
        map(a => a.reduce((acc, cur) => acc.concat(new Dashboard(cur))), []));
  }

  getDashboard(dashboardId: number): Observable<any> {
    return this.http.get(`${this.server}/rest/api/3/dashboard/${dashboardId}`,
      this.httpHeaders);
  }
}
