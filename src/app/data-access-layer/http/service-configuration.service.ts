import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ServiceConfigurationService {

  constructor() {
  }

  get server() {
    return localStorage.getItem('server');
  }

  set server(server: string) {
    localStorage.setItem('server', server);
  }

  get email() {
    return localStorage.getItem('email');
  }

  set email(email: string) {
    localStorage.setItem('email', email);
  }

  get token() {
    return localStorage.getItem('token');
  }

  set token(token: string) {
    localStorage.setItem('token', token);
  }

  get authHeader() {
    const auth = `${this.email}:${this.token}`;
    const encodedAuth = btoa(auth);
    return `Basic ${encodedAuth}`;
  }

  get httpHeaders() {
    return {
      headers: {
        Authorization: this.authHeader,
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'X-Atlassian-Token': 'no-check',
        'User-Agent': 'jira-light'
      }
    };
  }
}
