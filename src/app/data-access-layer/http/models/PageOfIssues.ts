import {BatchResult} from './BatchResult';
import {Ticket} from '../../../models/Ticket';

export class PageOfIssues extends BatchResult {
  constructor(o) {
    super(o);
  }

  get issues() {
    return this.source.issues.array.map(issue => new Ticket(issue));
  }
}
