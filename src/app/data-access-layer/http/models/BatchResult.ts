import {JiraObject} from '../../../shared/JiraObject';

export class BatchResult extends JiraObject {
  constructor(o) {
    super(o);
  }

  get maxResults(): number {
    return this.source.maxResults.value;
  }

  get startAt(): number {
    return this.source.startAt.value;
  }

  get total(): number {
    return this.source.total.value;
  }

  isLast: boolean;
  nextPage: string;
}
