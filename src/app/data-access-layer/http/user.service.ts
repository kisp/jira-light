import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {ServiceConfigurationService} from './service-configuration.service';

@Injectable({
  providedIn: 'root'
})
export class UserService extends ServiceConfigurationService {
  constructor(private http: HttpClient) {
    super();
  }

  myself(): Observable<any> {
    return this.http.get(`${this.server}/rest/api/3/myself`,
      {
        headers: {
          Authorization: this.authHeader,
          Accept: 'application/json',
          'Content-Type': 'application/json'
        }
      });
  }
}
