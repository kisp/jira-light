import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { MessageService } from 'primeng/api';
import { catchError, tap } from 'rxjs/operators';
import { Observable, of } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class HttpWithMonitorService {

    constructor(private http: HttpClient,
                private messageService: MessageService) {
    }

    put(url, body, options): Observable<any> {
        const jsonString = JSON.stringify(body);
        return this.logging(
            this.http.put(url, body, options),
            () => `PUT ${url}\n${jsonString}`);
    }

    post(url, body, options): Observable<any> {
        const jsonString = JSON.stringify(body);
        return this.logging(
            this.http.post(url, body, options),
            () => `POST ${url}\n${jsonString}`);
    }

    get(url, params) {
        return this.logging(
            this.http.get(url, params),
            () => `GET: ${url}&${new URLSearchParams(params.params).toString()}`
        );
    }

    private logging(o: Observable<any>, detailCb: () => string): Observable<any> {
        return o.pipe(
            catchError(err => {
                return of(err);
            }),
            tap(resp => {
                if (resp instanceof HttpErrorResponse) {
                    this.messageService.add({
                        severity: 'error',
                        summary: 'Http Error',
                        detail: resp.message
                    });
                } else {
                    this.messageService.add(({
                        severity: 'success',
                        summary: 'Http Success',
                        detail: detailCb()
                    }));
                }
            }));
    }
}
