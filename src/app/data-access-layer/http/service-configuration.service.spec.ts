import { TestBed } from '@angular/core/testing';

import { ServiceConfigurationService } from './service-configuration.service';

describe('ConfigurationService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ServiceConfigurationService = TestBed.get(ServiceConfigurationService);
    expect(service).toBeTruthy();
  });
});
