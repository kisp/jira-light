import { from, Observable, of, Subject } from 'rxjs';
import { concatMap, map, switchMap, zipAll } from 'rxjs/operators';
import { tick } from '@angular/core/testing';

type ExtractorFn = (input: any) => any[];

export const combinePaginateWithNextPageLink = (
    request: (url: string) => Observable<any>,
    url: string,
    extractCb: ExtractorFn = input => input,
    MAXIMUM = 200
): Observable<any> => {
    const paginate = (firstPageRequest: Observable<any> = null, accumulator = []): Observable<any> => firstPageRequest.pipe(
        concatMap(o => {
            const newResponse = [...accumulator, o];
            if (!o.isLast && newResponse.length < MAXIMUM) {
                const nextPage = o.nextPage.substring(o.nextPage.indexOf('/', 9));
                return paginate(
                    request(nextPage),
                    newResponse);
            }
            return of(newResponse);
        }));

    return paginate(request(url));
};

export const combinePaginate = <T, R extends {total: number, maxResults: number} > (
    request: (queryParams?) => Observable<R>,
    accumulatorFn: (head: T[], response: R) => T[],
    MAXIMUM = 200): Observable<T[]> => {
    let head: T[] = [];

    return request().pipe(
        switchMap(response => {
            const responses: Observable<R>[] = [of(response)];
            // create subpage requests
            head = accumulatorFn(head, response);

            if (head.length < response.total && head.length < MAXIMUM) {
                let startAt = response.maxResults;
                for (;
                     startAt < response.total && startAt < MAXIMUM;
                     startAt += response.maxResults) {
                    responses.push(
                        request({
                            startAt
                        }));
                }
            }

            return from(responses);
        }),
        zipAll(),
        map(arr => arr.reduce((acc, val) => accumulatorFn(acc, val), []))
    );
};

export const seemlessPaginate = <T>(
    request: (queryParams?) => Observable<any>,
    accumulatorFn,
    MAXIMUM = 200): Observable<T[]> => {
    let resp: T[] = [];
    const resp$: Subject<T[]> = new Subject();

    const sub = request().subscribe(response => {
        // create subpages reload
        resp = accumulatorFn(resp, response);
        resp$.next(resp);

        if (resp.length < response.total && resp.length < MAXIMUM) {
            for (let startAt = response.maxResults;
                 startAt < response.total && startAt < MAXIMUM;
                 startAt += response.maxResults) {
                request({
                    startAt
                }).subscribe(resp1 => {
                    resp = accumulatorFn(resp, resp1);
                    resp$.next(resp);
                });
            }
        }

        sub.unsubscribe();
    });

    return resp$.asObservable();
};
