import { TestBed } from '@angular/core/testing';

import { EntityManagerService } from './entity-manager.service';
import {HttpClient} from '@angular/common/http';
import {UserService} from './http/user.service';
import {TicketService} from './http/ticket.service';
import {FilterService} from './http/filter.service';
import {DashboardService} from './http/dashboard.service';
import {ProjectService} from './http/project.service';
import {GroupService} from './http/group.service';
import {NEVER} from 'rxjs';

describe('EntityManagerService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      {
        provide: HttpClient,
        useValue: {}
      },
      {
        provide: UserService,
        useValue: {
          myself: () => NEVER
        }
      },
      {
        provide: TicketService,
        useValue: {}
      },
      {
        provide: FilterService,
        useValue: {}
      },
      {
        provide: DashboardService,
        useValue: {}
      },
      {
        provide: ProjectService,
        useValue: {}
      },
      {
        provide: GroupService,
        useValue: {}
      }
    ]
  }));

  it('should be created', () => {
    const service: EntityManagerService = TestBed.get(EntityManagerService);
    expect(service).toBeTruthy();
  });
});
