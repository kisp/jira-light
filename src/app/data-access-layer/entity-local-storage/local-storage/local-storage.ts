import {IEntityStore} from '../IEntityStore';
import {ICanHandleKey} from '../ICanHandleKey';

export class LocalStorage implements IEntityStore<string, string>, ICanHandleKey {

  constructor() { }

  load(key: string): Promise<string> {
    const value = localStorage.getItem(`u.${key}`);
    return Promise.resolve(value);
  }

  save(key: string, value: string): Promise<string> {
    localStorage.setItem(`u.${key}`, value);
    return Promise.resolve(value);
  }

  canHandleKey(key: string): boolean {
    // all key is supported
    return true;
  }

  fetchMultiple(key: string): Promise<string[]> {
    const value = localStorage.getItem(`u.${key}`);
    return Promise.resolve(value.split(';'));
  }
}
