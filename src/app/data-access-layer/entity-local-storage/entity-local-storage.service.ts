import {Injectable} from '@angular/core';
import {IEntityStore} from './IEntityStore';
import {MessageService} from 'primeng/api';
import {LocalStorage} from './local-storage/local-storage';
import {JqlStorage} from './db/jql.storage';
import {ICanHandleKey} from './ICanHandleKey';

@Injectable({
  providedIn: 'root'
})
export class EntityLocalStorageService implements IEntityStore<string, string> {
  private stores: ICanHandleKey[];

  constructor(private messageService: MessageService) {
    this.initialize();
  }

  async load(k: string): Promise<string> {
    return this.findStoreForKey(k).load(k);
  }

  async save(k: string, entity: string): Promise<string> {
    return this.findStoreForKey(k).save(k, entity);
  }

  async fetchMultiple(k: string): Promise<string[]> {
    return this.findStoreForKey(k).fetchMultiple(k);
  }

  private initialize() {
    this.stores = [
      new JqlStorage(this.messageService),
      new LocalStorage()
    ] as unknown as ICanHandleKey[];
  }
  private findStoreForKey(key): IEntityStore<string, string> {
    return this.stores.find(store => store.canHandleKey(key)) as unknown as IEntityStore<string, string>;
  }
}
