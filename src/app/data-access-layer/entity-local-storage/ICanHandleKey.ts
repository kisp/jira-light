export interface ICanHandleKey {
  canHandleKey(key: string): boolean;
}
