export interface IEntityStore<Key, Entity> {
  load(k: Key): Promise<Entity>;
  save(k: Key, entity: Entity): Promise<Entity>;
  fetchMultiple(k: Key): Promise<Entity[]>;
}
