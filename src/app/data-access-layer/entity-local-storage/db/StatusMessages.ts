export const StatusMessages = {
  ACCESS_ERROR: {
    severity: 'error',
    summary: 'Can not access indexedDB!',
    detail: 'IndexedDB is not available on window!'
  },
  OPEN_SUCCESS: {
    severity: 'success',
    summary: 'IndexedDB opened successfully'
  },
  customError: (summary, detail) => ({
    severity: 'error',
    summary,
    detail
  }),
  customSuccess: (summary, detail) => ({
    severity: 'success', summary, detail
  })
};
