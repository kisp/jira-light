
export interface IJQL {
  id?: number;
  jql: string;
  timestamp: Date;
  feature: string;
  tokenizedJQLHash: string;
}
