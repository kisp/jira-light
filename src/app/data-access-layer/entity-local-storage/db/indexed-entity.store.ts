import {Message, MessageService} from 'primeng/api';
import {StatusMessages} from './StatusMessages';

declare global {
  interface Window {
    mozIndexedDB?: any;
    webkitIndexedDB?: any;
    msIndexedDB?: any;

    IDBTransaction?: any;
    webkitIDBTransaction?: any;
    msIDBTransaction?: any;
    IDBKeyRange?: any;
    webkitIDBKeyRange?: any;
    msIDBKeyRange?: any;
  }
}

class FakeDB {
  static INSTANCE = new FakeDB();

  onerror = () => {};
  open = () => setTimeout(this.onerror, 100);
}

export abstract class IndexedEntityStore {

  abstract messageService: MessageService;
  abstract storeName: string;
  abstract indexedColumnName: string;

  /* typeOf(version) unsigned long long*/
  abstract version: number;
  protected databaseName = 'MyDatabase';
  protected MAX_LIMIT = 100;

  private readonly indexedDB;

  abstract upgradeSchema(event: IDBVersionChangeEvent);

  protected constructor() {
    this.indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB || FakeDB.INSTANCE;
  }

  protected initialize() {
    this.openDatabase().then(() => this.messageService.add(StatusMessages.OPEN_SUCCESS));
  }

  private async openDatabase(): Promise<IDBDatabase> {
    return new Promise<IDBDatabase>((resolve, reject) => {
      const request: IDBOpenDBRequest = this.indexedDB.open(this.databaseName, this.version);
      request.onerror = () => {
        this.onError();
        reject('Could not connect indexedDB.');
      };
      request.onsuccess = (event: Event) => resolve((event.target as any).result);
      request.onupgradeneeded = (event: IDBVersionChangeEvent) => this.upgradeSchema(event);
    });
  }

  protected onError(msg: Message = StatusMessages.ACCESS_ERROR): Promise<Message> {
    this.messageService.add(msg);
    return Promise.reject(msg);
  }

  protected async insertNew<T>(o: T, uniqueIndexName: string): Promise<number | Message> {
    return this.openDatabase().then(
      db => new Promise<number>(
        (success, notInserted) => {
          const transaction = db.transaction([this.storeName], 'readwrite');
          const store = transaction.objectStore(this.storeName);
          const res = store.index(uniqueIndexName).count(o[uniqueIndexName]);
          transaction.onerror = notInserted;
          res.onsuccess = ev => {
            if (ev.type === 'success') {
              const count: number = (ev.target as any).result;
              if (count === 0) {
                const request = store.add(o);
                // returning the id
                request.onsuccess = insert => success((insert.target as any).result);
                return;
              }
            }
            notInserted('Not inserted!');
          };
        }).catch(
        err => this.onError(
          StatusMessages.customError('Error at adding', err))
      ));
  }

  protected async getLatest<T>(indexMatch: string): Promise<T | Message> {
    return this.openDatabase().then(
      db => new Promise<T>(
        (success, notFound) => {
          const index = db.transaction([this.storeName]).objectStore(this.storeName).index(this.indexedColumnName);
          const cursor = index.openCursor(indexMatch, 'prev');
          cursor.onerror = notFound;
          cursor.onsuccess = ev => {
            if (ev.type === 'success') {
              const cur: IDBCursorWithValue = (ev.target as any).result;
              if (cur) {
                success(cur.value);
                return;
              }
            }
            notFound('Not found!');
          };
        }).catch(
        err => this.onError(
          StatusMessages.customError('Error at querying', err))
      ));
  }

  protected async getAll<T>(indexMatch: string): Promise<T[] | Message> {
    return new Promise<T[]>(
      (success, notFound) => this.openDatabase().then(
        db => {
          const index = db.transaction([this.storeName]).objectStore(this.storeName).index(this.indexedColumnName);
          const idbRequest = index.getAll(indexMatch, this.MAX_LIMIT);
          idbRequest.onerror = notFound;
          idbRequest.onsuccess = ev => {
            if (ev.type === 'success') {
              const cur: T[] = (ev.target as any).result;
              if (cur) {
                success(cur);

                return;
              }
            }
            notFound('Not found!');
          };
        })).catch(
      err => this.onError(
        StatusMessages.customError('Error at querying', err.toLocaleString()))
    );
  }
}
