import {Token} from './Token';

class EquivalentSample {
  [source: string]: Array<string>;
}

type EquivalentSamples = Array<EquivalentSample>;

function checkEquality(samples: EquivalentSamples) {
  samples.forEach(
    sample => {
      const source = Object.keys(sample)[0];
      const vanilla = Token.uniqueIndex(source);
      sample[source].forEach(eq => expect(
        Token.uniqueIndex(eq)).toBe(vanilla));
    }
  );
}

describe('JQL tokenizer', () => {

  let jql = 'project in ("Fantastic and jira in light ") AND sprint in openSprints() OR COMPONENT = jira ORDER BY Rank ASC';

  describe('normalize', () => {
    it('should remove multiple spaces', () => {
      // GIVEN
      jql = '  a  b   c    d ';
      // WHEN
      jql = Token.uniqueIndex(jql);
      // THEN
      expect(jql).toBe('a b c d');
    });
  });

  describe('parsing a flat expression', () => {

    it('should distinct different expressions', () => {
      const hash1 = Token.uniqueIndex('a = b');
      const hash2 = Token.uniqueIndex( 'a=b AND c<d');

      expect(hash1).not.toBe(hash2);
    });

    it('should be commutative on =', () => {
      const samples: EquivalentSamples = [
        {
          'a = b': ['a=b', 'b = a', 'b=a']
        }
      ];

      checkEquality(samples);
    });

    it('should be commutative on AND', () => {
      const samples: EquivalentSamples = [
        {
          'a = b AND c > d AND k':
            [
              'k AND c>d AND  a=b ',
              'c>d AND k AND a=b'
            ]
        },
      ];

      checkEquality(samples);
    });


    it('should be commutative on AND and OR keeping precedence', () => {
      const samples: EquivalentSamples = [
        {
          'a = b AND c > d OR k':
            [
              'k OR c>d AND  a=b ',
              'k OR a=b AND c>d'
            ]
        },
      ];

      checkEquality(samples);
    });
  });

  describe('parsing a nested expression', () => {
    it('should support multiple levels', () => {
      const samples: EquivalentSamples = [
        {
          'a = b AND ( a = c OR d = c)': [
            '(a=c OR c=d) AND b=a',
            '  ( d=c OR c=a) AND (b=a)'
          ]
        }
      ];

      checkEquality(samples);
    });
  });
});
