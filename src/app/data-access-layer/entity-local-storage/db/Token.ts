interface IParsedJql {
    jql: string;
    length: number; // parsed token end
    readonly hash: string;
}

// FIXME: should be provided by JQL-parser
export class Token implements IParsedJql {

    // in precedence order
    static SEPARATORS = ['=', ',', ' AND ', ' OR ', ')', '(', ' IN '];
    precedenceLevel: number;
    length: number;
    jql: string;
    children?: Array<Token | string>;

    get hash(): string {
        return this.children.map(Token.unwrap).join(
            Token.SEPARATORS[this.precedenceLevel]);
    }

    static uniqueIndex(jql: string): string {
        const t = Token.wrap(jql) as Token;

        return t.hash;
    }

    private static normalize(jql: string): string {
        let s = jql.replace(/[ ]{2,}/g, ' ');
        s = s.replace(/ and /ig, ' AND ');
        s = s.replace(/ or /ig, ' OR ');
        s = s.replace(/ in\s?\(/ig, ' IN (');
        return s.trim();
    }

    private static wrap(a: string, precedenceLevel?: number): Token | string {
        if (precedenceLevel === -1) {
            return a;
        }
        const t = new Token();
        if (precedenceLevel === undefined) {
            t.jql = Token.normalize(a);
            t.precedenceLevel = Token.SEPARATORS.length - 1;
        } else {
            t.jql = a;
            t.precedenceLevel = precedenceLevel;
        }
        t.length = t.jql.length;
        t.children = Token.tokenize(t);
        return t;
    }

    private static unwrap(a: Token | string) {
        return a instanceof Token ? (a as Token).hash : a;
    }

    private static tokenize(t: Token): Array<Token | string> {
        if (t.precedenceLevel === 0
            && (t.jql.indexOf('=') > -1 ||
                t.jql.indexOf('>') > -1 ||
                t.jql.indexOf('<') > -1
            )) {
            t.jql = t.jql.replace(/ /g, '');
        }

        const separator = Token.SEPARATORS[t.precedenceLevel];

        if (t.jql.indexOf(separator) < 0 && t.precedenceLevel > 0) {
            t.precedenceLevel -= 1;
            return Token.tokenize(t);
        }

        const tokens: (Token | string)[] = [];
        let nextToken: Token | string;
        let length = 0;
        for (let start = 0;
             start < t.jql.length;
             start += nextToken.length + separator.length
        ) {
            const str = t.jql.substr(start);
            length = this.calculateExpressionLength(str, separator);
            nextToken = Token.wrap(str.substr(0, length), t.precedenceLevel - 1);
            tokens.push(nextToken);
        }
        if (tokens.length > 0) {
            tokens.sort(Token.comparatorFn);
        }
        return tokens;
    }

    private static comparatorFn(a: Token | string, b: Token | string): number {
        const aa = Token.unwrap(a);
        const bb = Token.unwrap(b);

        if (aa === bb) {
            return 0;
        }
        return aa < bb ? -1 : 1;
    }

    private static startsWithParenthesis(jql: string): boolean {
        return jql.length >= 2 && (jql[0] === '(' || jql[1] === '(');
    }

    private static endOfParenthesis(jql: string): number {
        const stack = [];
        for (let i = 0; i < jql.length; i++) {
            if (jql[i] === '(') {
                stack.push(i);
            } else if (jql[i] === ')') {
                stack.pop();
                if (stack.length === 0) {
                    return i;
                }
            }
        }
    }

    private static calculateExpressionLength(jql: string, separator: string) {
        if (this.startsWithParenthesis(jql)) {
            return this.endOfParenthesis(jql);
        }
        const ix = jql.indexOf(separator);
        if (ix < 0) {
            return jql.length;
        }
        return ix;
    }
}

