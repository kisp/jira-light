import { IEntityStore } from '../IEntityStore';
import { IndexedEntityStore } from './indexed-entity.store';
import { MessageService } from 'primeng/api';
import { StatusMessages } from './StatusMessages';
import { IJQL } from './IJQL';
import { ICanHandleKey } from '../ICanHandleKey';
import { Token } from './Token';

export class JqlStorage extends IndexedEntityStore implements IEntityStore<string, string>, ICanHandleKey {
  version = 6;
  storeName = 'queries';
  indexedColumnName = 'feature';

  messageService: MessageService;

  constructor(msgService: MessageService) {
    super();
    this.messageService = msgService;
    this.initialize();
  }

  async load(featureKey: string): Promise<string> {
    return this.getLatest(featureKey).then(
      (jql: IJQL) => jql.jql
    ).catch(() => 'sprint in openSprints()');
  }

  async save(featureKey: string, jqlStr: string): Promise<string> {
    const jql: IJQL = {
      tokenizedJQLHash: Token.uniqueIndex(jqlStr),
      feature: featureKey,
      jql: jqlStr,
      timestamp: new Date()
    };

    return this.insertNew(jql, 'jql')
      .then(n => String(n))
      .catch(() => '');
  }

  async fetchMultiple(featureKey: string): Promise<string[]> {
    return this.getAll(featureKey).then(
      (jqls: IJQL[]) => jqls.map(jql => jql.jql))
      .catch(() => []);
  }

  canHandleKey(key: string): boolean {
    return key === 'boardJql' || key === 'dependenciesJql';
  }

  upgradeSchema(event: IDBVersionChangeEvent) {
    const db: IDBDatabase = (event.target as any).result;

    this.messageService.add(
      StatusMessages.customSuccess('IndexedDB', `Upgrade schema to version: ${this.version}`));

    try {
      db.deleteObjectStore(this.storeName);
    } catch (e) {
      this.messageService.add(
        StatusMessages.customSuccess('IndexedDB', 'No previous object store to delete'));
    }

    const objectStore: IDBObjectStore = db.createObjectStore(this.storeName, {keyPath: 'id', autoIncrement: true});

    // TODO unique does not block to save duplicates
    objectStore.createIndex('jql', 'jql', {unique: true});
    objectStore.createIndex('timestamp', 'timestamp', {unique: false});
    objectStore.createIndex('feature', 'feature', {unique: false});

    objectStore.transaction.oncomplete = () => this.messageService.add(
      StatusMessages.customSuccess(
        `IndexedDB upgraded to version ${this.version}`,
        `A new indexed object store is ready for JQL objects`
      ));
  }

}
