import { Injectable } from '@angular/core';
import { Ticket } from '../models/Ticket';
import { forkJoin, Observable, of, zip } from 'rxjs';
import { TicketService } from './http/ticket.service';
import { catchError, map, mergeMap, shareReplay, switchMap } from 'rxjs/operators';
import { UserService } from './http/user.service';
import { Person } from '../models/Person';
import { FilterService } from './http/filter.service';
import { DashboardService } from './http/dashboard.service';
import { ProjectService } from './http/project.service';
import { Project } from '../models/Project';
import { GroupService } from './http/group.service';
import { Group } from '../models/Group';
import { Dashboard } from '../models/Dashboard';
import { TicketLink } from '../models/TicketLink';

/**
 * Purpose of this class is not to contain all the business logic,
 * but to handle an abstraction layer over the entities.
 * When two different entities are depend on each-other this is the place
 * what can connect them.
 *
 * See the antipattern anemic models
 */
@Injectable({
  providedIn: 'root'
})
export class EntityManagerService {

  private mySelf$: Observable<Person>;
  private projects$: Observable<Project[]>;

  constructor(
    private userService: UserService,
    private ticketService: TicketService,
    private filterService: FilterService,
    private dashboardService: DashboardService,
    private projectService: ProjectService,
    private groupService: GroupService) {

    this.mySelf$ = this.whoAmI().pipe(shareReplay(1));
  }

  whoAmI(): Observable<Person> {
    return this.userService.myself().pipe(
      map(o => new Person(o))
    );
  }

  // TODO: data-bind to models
  fetchMyFilters(): Observable<any> {
    return this.mySelf$
      .pipe(
        switchMap(p => this.filterService.searchFilters(p.id)));

  }

  fetchDashboardById(dashboardId: number): Observable<Dashboard> {
    return this.dashboardService.getDashboard(dashboardId)
      .pipe(
        map(o => new Dashboard(o))
      );
  }

  fetchAllGroupsDashboards(): Observable<Dashboard[]> {
    return this.fetchGroups().pipe(
      mergeMap(groups =>
        forkJoin(groups.map(g => this.dashboardService.searchDashboardsByGroupname(g.name)))
      ),
      map(a => a.reduce((acc, cur) => acc.concat(cur)), [])
    );
  }

  fetchProjects(): Observable<Project[]> {
    if (!this.projects$) {
      this.projects$ = this.projectService.getProjects().pipe(shareReplay(1));
    }
    return this.projects$;
  }

  fetchGroups(): Observable<Group[]> {
    return this.mySelf$.pipe(
      switchMap(p => this.groupService.search(p.id)),
      map(arr => arr.map(o => new Group(o)))
    );
  }

  findTicketsByJQL(jql: string): Observable<Ticket[]> {
    return this.ticketService.searchAllSeamless(jql);
  }

  findTicketsByJQLSync(jql: string): Observable<Ticket[]> {
    return this.ticketService.searchAllWaitForCombine(jql)
      .pipe(
        switchMap((tickets: Ticket[]) => {

          const ticketKeys: string[] = tickets.map(t => t.key);

          const missingLinks: TicketLink[] = tickets.map(t => t.linkedTickets)
            .reduce((acc, curr) => acc.concat(curr), [])
            .filter( (link: TicketLink) => !ticketKeys.includes(link.key));

          // unique
          return zip(... [... new Set(missingLinks.map(link => link.key))]
              .map( key => this.ticketService.getTicket(key)))
            .pipe(
            //  startWith(tickets) -> we do not want it to distribute in time
              map(newTickets => [...tickets, ...newTickets])
            );
        }));
  }

  fetchTicket(id: string): Observable<Ticket> {
    return this.ticketService.getTicket(id).pipe(
      catchError(err => {
        console.error(err);
        return of(Ticket.NULL);
      })
    );
  }

  assignToMe(issueKey: string): Observable<any> {
    return this.mySelf$.pipe(
      switchMap(p => this.ticketService.assignTo(issueKey, p.id)));
  }
}
