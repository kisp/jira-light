import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {AuthCredentialsComponent} from './auth-credentials.component';
import {FormsModule} from '@angular/forms';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {ServiceConfigurationService} from '../../data-access-layer/http/service-configuration.service';
import {UserSelectService} from '../../services/user-select.service';
import {of} from 'rxjs';
import {By} from '@angular/platform-browser';

class MockServiceConfig {
  email = 'email';
  token = 'token';
  server = 'server';
}

describe('AuthCredentialsComponent', () => {
  let component: AuthCredentialsComponent;
  let fixture: ComponentFixture<AuthCredentialsComponent>;

  const spies = {
    setJiraInstance: undefined,
    getJiraInstance: undefined
  };

  beforeEach(async(() => {

    spies.getJiraInstance = jasmine.createSpy('jiraInstance').and.callFake(() => of('initial-value'));
    spies.setJiraInstance = jasmine.createSpy('jiraInstance');

    TestBed.configureTestingModule({
      declarations: [
        AuthCredentialsComponent,
      ],
      imports: [FormsModule],
      providers: [
        {
          provide: ServiceConfigurationService,
          useClass: MockServiceConfig
        },
        {
          provide: UserSelectService,
          useValue: {
            userSelected: spies.getJiraInstance,
            userSet: spies.setJiraInstance
          }
        }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuthCredentialsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should save the changes of email form field', () => {
    // GIVEN
    const config = TestBed.get(ServiceConfigurationService);
    expect(config.email).toBe('email');
    // WHEN
    fixture.whenStable().then(() => {
      const input = fixture.debugElement.query(By.css('input'));
      const el = input.nativeElement;

      expect(el.value).toBe('email');

      el.value = 'email2';
      el.dispatchEvent(new Event('input'));

      fixture.detectChanges();
      // THEN
      expect(config.email).toBe('email2');
    });
  });
});
