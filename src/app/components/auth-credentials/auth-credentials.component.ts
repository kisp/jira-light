import {Component, OnInit} from '@angular/core';
import {ServiceConfigurationService} from '../../data-access-layer/http/service-configuration.service';
import {UserSelectService} from '../../services/user-select.service';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-auth-credentials',
  templateUrl: './auth-credentials.component.html',
  styleUrls: ['./auth-credentials.component.scss']
})
export class AuthCredentialsComponent implements OnInit {
  email: string;
  token: string;
  prefix: string;

  constructor(private config: ServiceConfigurationService,
              private settings: UserSelectService) {
  }

  ngOnInit() {
    this.email = this.config.email;
    this.token = this.config.token;
    this.prefix = this.config.server;
  }

  changeEmail(email: string) {
    this.config.email = this.email = email;
  }

  changeToken(token) {
    this.config.token = this.token = token;
  }

  changePrefix(server: string) {
    this.config.server = this.prefix = server;
  }

  get jiraInstance(): Observable<string> {
    return this.settings.userSelected('jiraInstance');
  }

  changeJiraInstance(instance: string) {
    this.settings.userSet('jiraInstance', instance);
  }
}
