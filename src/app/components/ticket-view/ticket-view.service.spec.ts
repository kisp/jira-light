import { TestBed } from '@angular/core/testing';

import { TicketViewService } from './ticket-view.service';
import { EntityManagerService } from '../../data-access-layer/entity-manager.service';
import { HttpClient } from '@angular/common/http';
import { MessageService } from 'primeng/api';
import { of } from 'rxjs';

describe('TicketViewService', () => {
    beforeEach(() => TestBed.configureTestingModule({
        providers: [
            {
                provide: EntityManagerService,
                useValue: {}
            },
            {
                provide: HttpClient,
                useValue: {
                    get: jasmine.createSpy('get').and.returnValue(of({}))
                }
            },
            {
                provide: MessageService,
                useValue: {add: jasmine.createSpy('add')}
            }
        ]
    }));

    it('should be created', () => {
        const service: TicketViewService = TestBed.get(TicketViewService);
        expect(service).toBeTruthy();
    });
});
