export class DisplayField {

  id: string;
  name: string;
  value: string;

  static create(id, name, value) {
    return new DisplayField(id, name, value);
  }

  constructor(id, name, value) {
    this.id = id;
    this.name = name;
    this.value = value;
  }
}
