import { Injectable } from '@angular/core';
import { EntityManagerService } from '../../data-access-layer/entity-manager.service';
import { debounce, map, switchMap } from 'rxjs/operators';
import { combineLatest, EMPTY, Observable, timer } from 'rxjs';
import { Ticket } from '../../models/Ticket';
import { DisplayField } from './DisplayField';
import { TicketService } from '../../data-access-layer/http/ticket.service';
import { Transition } from '../../models/Transition';
import { UserSelectService } from '../../services/user-select.service';

@Injectable({
  providedIn: 'root'
})
export class TicketViewService {

  constructor(private em: EntityManagerService, private ticketService: TicketService, private settings: UserSelectService) {
  }

  getTicket(ticketId$: Observable<string>): Observable<[Ticket, DisplayField[], Transition[]]> {
    return ticketId$.pipe(
      debounce( value => value ? timer(1000) : EMPTY),
      switchMap( id =>
          combineLatest([
            this.em.fetchTicket(id),
            this.ticketService.getTransitions(id)])),
      map(([ticket, transitions]) => [ticket, this.collectDisplayFields(ticket), transitions])
    );
  }

  private collectDisplayFields(ticket: Ticket): DisplayField[] {
    const f: DisplayField[] = [];

    ticket.fieldDefinitions.forEach(cf => {
      const val = ticket.fields[cf.id];

      if (typeof val === 'string' || typeof val === 'number') {
        f.push(DisplayField.create(cf.id, cf.name, String(val)));
      }

      // TODO: add formatters here
    });

    return f;
  }


  getNavigateExternalLink(ticketKey: string): Observable<string> {
    return this.settings.userSelected('jiraInstance')
      .pipe(
        map( urlPrefix => `${urlPrefix}/browse/${ticketKey}`));
  }

  async transitionTo(ticketId: string, transition: Transition): Promise<any> {
    return this.ticketService.transitionTo(ticketId, String(transition.id)).toPromise();
  }

  async assignToMe(ticketId: string): Promise<any> {
    return this.em.assignToMe(ticketId).toPromise();
  }
}
