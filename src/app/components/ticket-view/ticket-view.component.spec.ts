import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TicketViewComponent } from './ticket-view.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TicketViewService } from './ticket-view.service';
import { NEVER } from 'rxjs';

describe('TicketViewComponent', () => {
    let component: TicketViewComponent;
    let fixture: ComponentFixture<TicketViewComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [TicketViewComponent],
            providers: [
                {
                    provide: ActivatedRoute,
                    useValue: {
                        queryParams: NEVER
                    }
                },
                {
                    provide: TicketViewService,
                    useValue: {
                        getTicket: () => NEVER
                    }
                },
                {
                    provide: Router,
                    useValue: {
                        navigate: jasmine.createSpy('navigate')
                    }
                }
            ],
            schemas: [NO_ERRORS_SCHEMA]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(TicketViewComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
