import { AfterViewInit, ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BehaviorSubject, Observable, of, Subject, Subscription } from 'rxjs';
import { TicketViewService } from './ticket-view.service';
import { Ticket } from '../../models/Ticket';
import { DisplayField } from './DisplayField';
import { Transition } from '../../models/Transition';

@Component({
  selector: 'app-ticket-view',
  templateUrl: './ticket-view.component.html',
  styleUrls: ['./ticket-view.component.scss']
})
export class TicketViewComponent implements OnInit, OnDestroy, AfterViewInit {

  ticketKey: string;

  sub: Subscription;
  ticketNumber$: Subject<string>;

  ticket$: BehaviorSubject<Ticket>;
  fields$: BehaviorSubject<DisplayField[]>;
  transitions$: BehaviorSubject<Transition[]>;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private tickets: TicketViewService,
              private cd: ChangeDetectorRef) {
    this.sub = new Subscription();
    this.ticketNumber$ = new Subject<string>();
    this.ticket$ = new BehaviorSubject<Ticket>(Ticket.NULL);
    this.fields$ = new BehaviorSubject<DisplayField[]>([]);
    this.transitions$ = new BehaviorSubject<Transition[]>([]);
  }

  get ticket() {
    return this.ticket$.getValue();
  }

  get fields() {
    return this.fields$.getValue();
  }

  get transitions() {
    return this.transitions$.getValue();
  }

  get externalLink(): Observable<string> {
    if (this.ticket) {
      return this.tickets.getNavigateExternalLink(this.ticket.key);
    }
    return of('');
  }

  ngOnInit() {
    this.sub.add(
      this.tickets.getTicket(this.ticketNumber$).subscribe(([ticket, fields, transitions]) => {
        this.ticket$.next(ticket);
        this.fields$.next(fields);
        this.transitions$.next(transitions);

        this.cd.detectChanges();
      }));
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }

  ngAfterViewInit(): void {
    this.sub.add(
      this.route.queryParams.subscribe(
        p => {
          this.ticketKey = p.q;
          this.ticketNumber$.next(p.q);

          this.cd.detectChanges();
        })
    );
  }

  transitionTo(transition: Transition) {
    this.tickets.transitionTo(this.ticket.key, transition).then(() => this.refresh());
  }

  assignToMe() {
    this.tickets.assignToMe(this.ticket.key).then(() => this.refresh());
  }

  ticketKeyKeypress($event: KeyboardEvent) {
    if ($event.key === 'Enter') {
      this.router.navigate(['/ticket'], {
        queryParams: {
          q: this.ticketKey
        }
      });
    }
  }

  private refresh() {
    this.ticketNumber$.next(this.ticket.key);
  }
}
