import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JqlInputComponent } from './jql-input.component';
import { MessageService } from 'primeng/api';

describe('JqlInputComponent', () => {
    let component: JqlInputComponent;
    let fixture: ComponentFixture<JqlInputComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [JqlInputComponent],
            providers: [{provide: MessageService, useValue: {add: jasmine.createSpy('add')}}]
        })
            .compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(JqlInputComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
