import { ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { UserSelectService } from '../../../../../services/user-select.service';

@Component({
  selector: 'app-jql-selector',
  templateUrl: './jql-selector.component.html',
  styleUrls: ['./jql-selector.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class JqlSelectorComponent implements OnInit {

  @Input()
  selectorKey: string;

  @Output()
  selected: EventEmitter<string>;
  opened: boolean;
  jqls: Array<string> = [];

  constructor(private storage: UserSelectService,
              private cd: ChangeDetectorRef) {
    this.opened = false;
    this.selected = new EventEmitter<string>();
  }

  ngOnInit() {
  }

  toggle() {
    this.opened = !this.opened;

    if (this.opened) {
      this.storage.fetchMultiple(this.selectorKey)
        .then(results => {
          this.jqls = results;
          this.cd.detectChanges();
        });
    }
  }

  get label() {
    return this.opened ? 'Close' : 'Pick';
  }

  clicked($event: MouseEvent) {
    const clickedValue = ($event.target as HTMLElement).textContent;
    this.selected.emit(clickedValue);
    this.toggle();
  }
}
