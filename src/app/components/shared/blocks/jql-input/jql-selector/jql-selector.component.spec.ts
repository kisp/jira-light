import { ComponentFixture, TestBed } from '@angular/core/testing';
import { JqlSelectorComponent } from './jql-selector.component';
import { MessageService } from 'primeng/api';

describe('JqlSelectorComponent', () => {
    let component: JqlSelectorComponent;
    let fixture: ComponentFixture<JqlSelectorComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [JqlSelectorComponent],
            providers: [
                {
                    provide: MessageService,
                    useValue: {
                        add: jasmine.createSpy('add')
                    }
                }
            ]
        })
            .compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(JqlSelectorComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
