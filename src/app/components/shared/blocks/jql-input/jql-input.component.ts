import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { Ticket } from '../../../../models/Ticket';
import { UserSelectService } from '../../../../services/user-select.service';
import { parse } from 'jql-parser';
import { take } from 'rxjs/operators';

@Component({
  selector: 'app-jql-input',
  templateUrl: './jql-input.component.html',
  styleUrls: ['./jql-input.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class JqlInputComponent implements OnInit, OnDestroy {

  @Input()
  selectorKey: string;

  sub: Subscription = new Subscription();
  tickets: Ticket[];
  jql: string;

  private jql$: Observable<string>;
  constructor(
    private cd: ChangeDetectorRef,
    private storage: UserSelectService) {
  }

  get validJql(): boolean {
    return !!this.jql ? parse(this.jql) : false;
  }

  ngOnInit() {
    this.jql$ = this.storage.userSelected(this.selectorKey);

    this.sub.add(this.jql$.pipe(take(1)).subscribe(jql => {
      this.jql = jql;
      this.cd.detectChanges();
    }));
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }

  triggerSearch($event: KeyboardEvent) {
    this.storage.userSet(this.selectorKey, this.jql, false);
  }

  selectSavedJQL(jql: string) {
    this.jql = jql;
    this.storage.userSet(this.selectorKey, this.jql, false);
    this.cd.detectChanges();
  }

  jqlInputChange($event: Event) {
    this.jql = ($event.target as HTMLInputElement).value;
    this.cd.detectChanges();
  }
}
