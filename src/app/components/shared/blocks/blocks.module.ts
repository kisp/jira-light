import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { JqlSelectorComponent } from './jql-input/jql-selector/jql-selector.component';
import { ButtonModule } from 'primeng/button';
import { InputTextModule } from 'primeng/inputtext';
import { DialogModule } from 'primeng/dialog';
import { JqlInputComponent } from './jql-input/jql-input.component';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    JqlSelectorComponent,
    JqlInputComponent
  ],
  exports: [
    JqlSelectorComponent,
    JqlInputComponent
  ],
  imports: [
    CommonModule,
    ButtonModule,
    InputTextModule,
    DialogModule,
    FormsModule

  ]
})
export class BlocksModule { }
