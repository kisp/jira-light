import { Component, OnInit } from '@angular/core';
import {FilterService} from '../../data-access-layer/http/filter.service';
import {UserService} from '../../data-access-layer/http/user.service';
import {EntityManagerService} from '../../data-access-layer/entity-manager.service';

@Component({
  selector: 'app-filters-view',
  templateUrl: './filters-view.component.html',
  styleUrls: ['./filters-view.component.scss']
})
export class FiltersViewComponent implements OnInit {

  constructor(private em: EntityManagerService) { }

  ngOnInit() {
    this.em.fetchMyFilters().subscribe(console.log);
    // this.filters.searchFilters().subscribe();
  }

}
