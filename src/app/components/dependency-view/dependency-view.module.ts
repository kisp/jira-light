import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DependencyViewComponent } from './dependency-view.component';
import { BlocksModule } from '../shared/blocks/blocks.module';
import { GanttWrapperComponent } from './gantt-wrapper/gantt-wrapper.component';
import { DependencyRoutingModule } from './dependency-routing-module';
import { EmbedAnchorDirective } from './embed-anchor.directive';

@NgModule({
    declarations: [DependencyViewComponent, GanttWrapperComponent, EmbedAnchorDirective],
    imports: [CommonModule, BlocksModule, DependencyRoutingModule],
    entryComponents: [ GanttWrapperComponent ]
})
export class DependencyViewModule {
}
