import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DependencyViewComponent } from './dependency-view.component';

const routes: Routes = [
    {
        path: '',
        component: DependencyViewComponent,
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class DependencyRoutingModule {}
