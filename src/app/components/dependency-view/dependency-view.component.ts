import {
    ChangeDetectorRef,
    Component,
    ComponentFactoryResolver,
    ComponentRef,
    OnDestroy,
    OnInit,
    ViewChild
} from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { Ticket } from '../../models/Ticket';
import { UserSelectService } from '../../services/user-select.service';
import { EntityManagerService } from '../../data-access-layer/entity-manager.service';
import { EntityLocalStorageService } from '../../data-access-layer/entity-local-storage/entity-local-storage.service';
import { switchMap, tap } from 'rxjs/operators';
import { GanttWrapperComponent } from './gantt-wrapper/gantt-wrapper.component';
import { EmbedAnchorDirective } from './embed-anchor.directive';

@Component({
    selector: 'app-dependency-view',
    templateUrl: './dependency-view.component.html',
    styleUrls: ['./dependency-view.component.scss'],
})
export class DependencyViewComponent implements OnInit, OnDestroy {
    @ViewChild(EmbedAnchorDirective, {static: true})
    host: EmbedAnchorDirective;
    ticketCount = 0;
    selectorKey = 'dependenciesJql';
    private sub: Subscription = new Subscription();
    private componentRef: ComponentRef<GanttWrapperComponent>;
    private jql$: Observable<string>;
    private tickets$: Observable<Ticket[]>;
    private lastJQL: string;

    constructor(
        private cd: ChangeDetectorRef,
        private storage: UserSelectService,
        private em: EntityManagerService,
        private jqlStorage: EntityLocalStorageService,
        private componentFactoryResolver: ComponentFactoryResolver
    ) {
    }

    ngOnInit() {
        this.jql$ = this.storage.userSelected(this.selectorKey);

        this.tickets$ = this.jql$.pipe(
            tap(jql => (this.lastJQL = jql)),
            tap(() => {
                const componentFactory = this.componentFactoryResolver.resolveComponentFactory(GanttWrapperComponent);
                this.host.viewContainerRef.clear();
                this.componentRef = this.host.viewContainerRef.createComponent(componentFactory);
                this.ticketCount = 0;

            }),
            switchMap(jql => this.em.findTicketsByJQLSync(jql)),
        );

        this.sub.add(
            this.tickets$
                .pipe(tap(tickets => (this.ticketCount = tickets.length)))
                .subscribe(tickets => {
                    this.componentRef.instance.tickets = tickets;
                    // if (tickets.length) {
                    this.jqlStorage.save(this.selectorKey, this.lastJQL);
                    // }
                    this.cd.detectChanges();
                }),
        );
    }

    ngOnDestroy(): void {
        this.sub.unsubscribe();
    }
}
