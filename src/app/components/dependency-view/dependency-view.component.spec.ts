import { ComponentFixture, TestBed } from '@angular/core/testing';
import { DependencyViewComponent } from './dependency-view.component';
import { MessageService } from 'primeng/api';
import { HttpClient } from '@angular/common/http';
import { of } from 'rxjs';

describe('DependencyViewComponent', () => {
    let component: DependencyViewComponent;
    let fixture: ComponentFixture<DependencyViewComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [DependencyViewComponent],
            providers: [
                {
                    provide: MessageService,
                    useValue: {
                        add: jasmine.createSpy('add')
                    }
                },
                {
                    provide: HttpClient,
                    useValue: { get: jasmine.createSpy('get').and.returnValue(of({}))}
                }
            ]
        })
            .compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(DependencyViewComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
