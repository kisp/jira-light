import { Directive, Input, ViewContainerRef } from '@angular/core';

@Directive({
    selector: '[appEmbedAnchor]'
})
export class EmbedAnchorDirective {

    @Input()
    appEmbedAnchor: string;

    constructor(public viewContainerRef: ViewContainerRef) {
    }

}
