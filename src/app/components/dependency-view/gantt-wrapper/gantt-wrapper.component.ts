import {
  AfterViewChecked,
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  Input,
  OnInit,
  ViewChild,
} from '@angular/core';
import { Ticket } from '../../../models/Ticket';

declare global {
    interface Window {
        Gantt: any;
    }
}

const Gantt = window.Gantt;

const Config = {
    header_height: 50,
    column_width: 30,
    step: 24,
    view_modes: ['Quarter Day', 'Half Day', 'Day', 'Week', 'Month'],
    bar_height: 20,
    bar_corner_radius: 3,
    arrow_curve: 5,
    padding: 18,
    view_mode: 'Day',
    date_format: 'YYYY-MM-DD',
    custom_popup_html: null,
};

@Component({
    selector: 'app-gantt-wrapper',
    templateUrl: './gantt-wrapper.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class GanttWrapperComponent implements OnInit, AfterViewChecked {
    @ViewChild('gantt', { static: true })
    gantt: ElementRef;

    @Input()
    tickets: Ticket[] = [];

    private g: any;

    constructor() {}

    ngOnInit() {}

    get transformedData(): any[] {
        return this.tickets.map(t => ({
            id: t.key,
            name: `${t.key} - ${t.title}`,
            dependencies: t.outwardKeys.join(','),
            progress: 0,
            start: t.created,
            end: t.updated,
        }));
    }

    ngAfterViewChecked(): void {
        if (!this.g && Array.isArray(this.tickets) && this.tickets.length > 0) {
            this.g = new Gantt(this.gantt.nativeElement, this.transformedData, Config);
        }
    }
}
