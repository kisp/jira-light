import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BoardComponent } from './board.component';
import { BoardViewComponent } from './view/board-view.component';
import { TicketComponent } from './view/ticket/ticket.component';
import { ColumnHeaderComponent } from './view/column-header/column-header.component';
import { SafeStatusPipe } from './view/safe-status.pipe';
import { LeaderboardViewComponent } from './view/leaderboard-view/leaderboard-view.component';
import { BoardRoutingModule } from './board-routing.module';
import { FormsModule } from '@angular/forms';
import { CardModule } from 'primeng/card';
import { ButtonModule } from 'primeng/button';
import { TooltipModule } from 'primeng/tooltip';
import { ToastModule } from 'primeng/toast';
import { InputTextModule } from 'primeng/inputtext';
import { DialogModule } from 'primeng/dialog';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { BlocksModule } from '../shared/blocks/blocks.module';

@NgModule({
  declarations: [
    BoardComponent,
    BoardViewComponent,
    TicketComponent,
    ColumnHeaderComponent,
    SafeStatusPipe,
    LeaderboardViewComponent
  ],
  imports: [
    CommonModule,
    BoardRoutingModule,
    FormsModule,
    CardModule,
    TooltipModule,
    ToastModule,
    ButtonModule,
    InputTextModule,
    DialogModule,
    AutoCompleteModule,
    BlocksModule
  ]
})
export class BoardModule {
}
