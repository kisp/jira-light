import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BoardComponent } from './board.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ColumnsService } from './view/columns.service';
import { EntityManagerService } from '../../data-access-layer/entity-manager.service';
import { UserSelectService } from '../../services/user-select.service';
import { of, Subject } from 'rxjs';
import { MessageService } from 'primeng/api';

describe('BoardComponent', () => {
    let component: BoardComponent;
    let fixture: ComponentFixture<BoardComponent>;
    let storageSpy;
    let storedJql: Subject<string>;
    let resetColumnsSpy;
    let serviceCallSpy;
    let storageSetSpy;

    beforeEach(async(() => {
        storedJql = new Subject<string>();
        storageSpy = jasmine.createSpy('jql').and.returnValue(storedJql.asObservable());
        resetColumnsSpy = jasmine.createSpy('resetColumns');
        serviceCallSpy = jasmine.createSpy('findTicketsByJQL').and.returnValue(of([]));
        storageSetSpy = jasmine.createSpy('storageUserSet');

        TestBed.configureTestingModule({
            declarations: [BoardComponent],
            imports: [],
            providers: [
                {
                    provide: UserSelectService,
                    useValue: {
                        userSelected: storageSpy,
                        userSet: storageSetSpy,
                    }
                },
                {
                    provide: ColumnsService,
                    useValue: {
                        resetColumns: resetColumnsSpy
                    }
                },
                {
                    provide: EntityManagerService,
                    useValue: {
                        findTicketsByJQL: serviceCallSpy
                    }
                },
                {
                    provide: MessageService,
                    useValue: {
                        add: jasmine.createSpy('add')
                    }
                }
            ],
            schemas: [NO_ERRORS_SCHEMA]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(BoardComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should reset columns and store jql when the jql changes', (done) => {
        // GIVEN
        expect(resetColumnsSpy).not.toHaveBeenCalled();
        // WHEN
        storedJql.next('blabla');
        fixture.detectChanges();
        // THEN
        expect(resetColumnsSpy).toHaveBeenCalled();
        expect(storageSetSpy).toHaveBeenCalledWith(
            'boardJql', 'blabla', true);
        done();
    });
});
