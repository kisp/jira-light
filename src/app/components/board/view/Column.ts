import {Ticket} from '../../../models/Ticket';
import {Person} from '../../../models/Person';

export interface Leader {
  points: number;
  person: Person;
}

export class Column {
  static NULL = new Column('');
  private tickets: Ticket[];
  private members: Leader[];

  name: string;
  storyPoints: number;

  constructor(statusName: string) {
    this.name = statusName;
    this.reset();
  }

  matches(ticketStatus: string): boolean {
    return ticketStatus === this.name;
  }

  isEmpty(): boolean {
    return !this.tickets.length;
  }

  getLeaderboardMembers() {
    return this.members.sort((ticket1, ticket2) => ticket2.points - ticket1.points).slice(0, 3);
  }

  add(ticket: Ticket) {
    if (!this.findTicket(ticket)) {
      this.addNewTicket(ticket);
    }
  }

  reset() {
    this.storyPoints = 0;
    this.tickets = [];
    this.members = [];
  }

  private addNewTicket(ticket: Ticket) {
    this.tickets.push(ticket);
    if (typeof ticket.storyPoints === 'number') {
      this.storyPoints += ticket.storyPoints;

      const index = this.members.findIndex(m => m.person.id === ticket.assignee.id);

      if (index > -1) {
        this.members[index].points += ticket.storyPoints;
      } else {
        this.members.push({ person: ticket.assignee, points: ticket.storyPoints });
      }
    }
  }

  private findTicket(ticket: Ticket) {
    return this.tickets.find(t => t.key === ticket.key);
  }
}
