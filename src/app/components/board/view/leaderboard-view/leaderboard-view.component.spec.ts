import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {LeaderboardViewComponent} from './leaderboard-view.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {ColumnsService} from '../columns.service';
import {NEVER} from 'rxjs';

describe('LeaderboardViewComponent', () => {
  let component: LeaderboardViewComponent;
  let fixture: ComponentFixture<LeaderboardViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LeaderboardViewComponent],
      providers: [
        {
          provide: ColumnsService,
          useValue: {
            changes: () => NEVER
          }
        }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeaderboardViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
