import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import {ColumnsService} from '../columns.service';
import {Leader} from '../Column';

@Component({
  selector: 'app-leaderboard-view',
  templateUrl: './leaderboard-view.component.html',
  styleUrls: ['./leaderboard-view.component.scss']
})
export class LeaderboardViewComponent implements OnInit, OnDestroy {
  @Input() statusName: string;

  private subscriptions: Subscription;

  first: Leader;
  second: Leader;
  third: Leader;

  constructor(private columnService: ColumnsService) {
    this.subscriptions = new Subscription();
  }

  ngOnInit() {
    this.subscriptions.add(this.columnService.changes(this.statusName).subscribe(col => {
      const members = col.getLeaderboardMembers();

      this.first = members[0];
      this.second = members[1];
      this.third = members[2];
    }));
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }

}
