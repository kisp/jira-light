import {Injectable} from '@angular/core';
import {TicketStatus} from '../../../models/TicketStatus';
import {Observable, Subject} from 'rxjs';
import {UserSelectService} from '../../../services/user-select.service';
import {distinctUntilChanged, filter} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class OrderedStatusService {

  private storedOrder: string[] = [];
  private columns$: Subject<string[]>;

  constructor(private db: UserSelectService) {
    db.userSelected('status-order')
      .pipe(
        distinctUntilChanged(),
        filter(o => typeof o === 'string'))
      .subscribe(
        o => {
          this.storedOrder = o.split(';');
          this.storedOrder = this.storedOrder.filter(c => c.length > 0);
        }
      );
    this.columns$ = new Subject<string[]>();

    this.columns$.asObservable()
      .pipe(
        filter(o => Array.isArray(o)),
        distinctUntilChanged(
          (a1, a2) => a1.every((v, i) => a2[i] === v)
        )
      )
      .subscribe(
        o => db.userSet('status-order', o.join(';')));
  }

  get orderedStatuses$(): Observable<string[]> {
    return this.columns$.asObservable().pipe(
      filter(o => Array.isArray(o)),
      distinctUntilChanged(
        (a1, a2) => a1.every((v, i) => a2[i] === v)
      )
    );
  }

  append(status: TicketStatus) {
    if (!this.storedOrder.includes(status.name)) {
      this.storedOrder.push(status.name);
    }
    this.columns$.next([...this.storedOrder]);
  }

  private swap(ix1, ix2) {
    const swap = this.storedOrder[ix1];
    this.storedOrder[ix1] = this.storedOrder[ix2];
    this.storedOrder[ix2] = swap;
    this.storedOrder = [...this.storedOrder];

    this.columns$.next(this.storedOrder);
  }

  moveLeft(status: string) {
    const ix = this.storedOrder.indexOf(status);
    if (ix > 0) {
      this.swap(ix, ix - 1);
    }
  }

  moveRight(status: string) {
    const ix = this.storedOrder.indexOf(status);
    if (ix < this.storedOrder.length - 1) {
      this.swap(ix, ix + 1);
    }
  }

  remove(status: string) {
    const ix = this.storedOrder.indexOf(status);
    if (ix > -1) {
      this.storedOrder.splice(ix, 1);
      this.storedOrder = [...this.storedOrder];
      this.columns$.next(this.storedOrder);
    }
  }
}
