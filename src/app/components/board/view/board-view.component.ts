import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnDestroy, OnInit } from '@angular/core';
import {Subscription} from 'rxjs';
import {Ticket} from '../../../models/Ticket';
import {DomSanitizer, SafeStyle} from '@angular/platform-browser';
import {SafeStatusPipe} from './safe-status.pipe';
import {ColumnsService} from './columns.service';

@Component({
  selector: 'app-board-view',
  templateUrl: './board-view.component.html',
  styleUrls: ['./board-view.component.scss'],
  providers: [SafeStatusPipe],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BoardViewComponent implements OnInit, OnDestroy {

  @Input()
  tickets: Ticket[];
  statuses: string[];
  gridColumnTemplate: SafeStyle;

  private subscription: Subscription;

  constructor(private sanitizer: DomSanitizer,
              private columnsService: ColumnsService,
              private safeStatus: SafeStatusPipe,
              private cd: ChangeDetectorRef) {
    this.subscription = new Subscription();
    this.tickets = [];
  }

  ngOnInit() {

    this.subscription.add(
      this.columnsService.orderedStatuses$
        .subscribe(columns => {
            this.statuses = columns;

            const columnTemplateStr = columns.map(status => `[${this.safeStatus.transform(status)}] 1fr`).join(' ');
            this.gridColumnTemplate = this.sanitizer.bypassSecurityTrustStyle(columnTemplateStr);

            this.cd.detectChanges();
          }
        ));
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  ticketKeyFn(index, item: Ticket) {
    return item.key;
  }
}
