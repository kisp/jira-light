import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {TicketComponent} from './ticket.component';
import {RouterTestingModule} from '@angular/router/testing';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {UserSelectService} from '../../../../services/user-select.service';
import {Ticket} from '../../../../models/Ticket';
import {Subject} from 'rxjs';


describe('TicketComponent', () => {
  let component: TicketComponent;
  let fixture: ComponentFixture<TicketComponent>;

  let userSelection: Subject<string>;
  let userSelectionSpy;

  beforeEach(async(() => {
    userSelection = new Subject();
    userSelectionSpy = jasmine.createSpy('userSelected').and.callFake(() => userSelection.asObservable());

    TestBed.configureTestingModule({
      declarations: [
        TicketComponent,
      ],
      imports: [
        RouterTestingModule,
      ],
      providers: [
        {
          provide: UserSelectService,
          useValue: {
            userSelected: userSelectionSpy
          }
        }
      ],
      schemas: [ NO_ERRORS_SCHEMA ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TicketComponent);
    component = fixture.componentInstance;
    component.ticket = {
      key: 'TEST-001',
      storyPoints: 13,
      reporter: { },
      assignee: { }
    } as Ticket;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
