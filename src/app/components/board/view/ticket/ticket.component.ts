import { ChangeDetectionStrategy, Component, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { Ticket } from '../../../../models/Ticket';
import { UserSelectService } from '../../../../services/user-select.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-ticket',
  templateUrl: './ticket.component.html',
  styleUrls: ['./ticket.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TicketComponent implements OnInit {

  @Input()
  ticket: Ticket;

  constructor(private settings: UserSelectService) { }

  ngOnInit() {
  }

  get storyPoints(): string {
    return this.ticket.storyPoints ? String(this.ticket.storyPoints) : '-';
  }

  get navigateExternalLink(): Observable<string> {
    return this.settings.userSelected('jiraInstance')
      .pipe(
        map( urlPrefix => `${urlPrefix}/browse/${this.ticket.key}`));
  }
}
