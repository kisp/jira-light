import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {BoardViewComponent} from './board-view.component';
import {ColumnHeaderComponent} from './column-header/column-header.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {SafeStatusPipe} from './safe-status.pipe';
import {ColumnsService} from './columns.service';
import {of} from 'rxjs';

describe('Board-view component', () => {
  let component: BoardViewComponent;
  let fixture: ComponentFixture<BoardViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        BoardViewComponent,
        ColumnHeaderComponent,
        SafeStatusPipe
      ],
      providers: [
        {
          provide: ColumnsService,
          useValue: {
            orderedStatuses$: of([])
          }
        }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BoardViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
