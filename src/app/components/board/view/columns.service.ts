import {Injectable} from '@angular/core';
import {Column} from './Column';
import {Ticket} from '../../../models/Ticket';
import {Observable, ReplaySubject, Subject} from 'rxjs';
import {OrderedStatusService} from './ordered-status.service';
import {TicketStatus} from '../../../models/TicketStatus';

@Injectable({
  providedIn: 'root'
})
export class ColumnsService {
  private columns: Column[] = [];
  private subscribers: Map<string, Subject<Column>>;

  // TODO: remove OSS
  constructor(private orderedStatusService: OrderedStatusService) {
    this.subscribers = new Map<string, Subject<Column>>();
  }

  update(t: Ticket) {
    const statusName = t.status.name;
    let column: Column = this.findColumn(statusName);
    if (!column) {
      column = new Column(statusName);
      this.columns.push(column);
    }
    column.add(t);
    this.getOrCreateSubject(statusName).next(column);
  }

  findColumn(ticketStatus: string) {
    return this.columns.find((c: Column) => c.matches(ticketStatus));
  }

  changes(ticketStatus: string): Observable<Column> {
    return this.getOrCreateSubject(ticketStatus).asObservable();
  }

  private getOrCreateSubject(ticketStatus: string): Subject<Column> {
    if (!this.subscribers.has(ticketStatus)) {
      const subject = new ReplaySubject<Column>(1);
      this.subscribers.set(ticketStatus, subject);
    }
    return this.subscribers.get(ticketStatus);
  }

  resetColumns() {
    this.columns.forEach(column => {
      column.reset();
      this.getOrCreateSubject(column.name).next(column);
    });
  }

  // TODO: refactor step2: make it possible to store column order instead!

  get orderedStatuses$(): Observable<string[]> {
    return this.orderedStatusService.orderedStatuses$;
  }

  moveLeft(status: string) {
    this.orderedStatusService.moveLeft(status);
  }

  moveRight(status: string) {
    this.orderedStatusService.moveRight(status);
  }

  remove(status: string) {
    this.orderedStatusService.remove(status);
  }

  append(status: TicketStatus) {
    this.orderedStatusService.append(status);
  }
}
