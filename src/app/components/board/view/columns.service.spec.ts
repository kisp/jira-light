import { TestBed } from '@angular/core/testing';

import { ColumnsService } from './columns.service';
import {OrderedStatusService} from './ordered-status.service';

describe('ColumnsService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      {
        provide: OrderedStatusService,
        useValue: {

        }
      }
    ]
  }));

  it('should be created', () => {
    const service: ColumnsService = TestBed.get(ColumnsService);
    expect(service).toBeTruthy();
  });

  // TODO test the public-api
});
