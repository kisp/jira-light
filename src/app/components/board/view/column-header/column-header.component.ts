import { ChangeDetectionStrategy, Component, Input, OnChanges, OnDestroy, OnInit, SimpleChanges } from '@angular/core';
import { ColumnsService } from '../columns.service';
import { Subject, Subscription } from 'rxjs';
import { Column } from '../Column';

@Component({
  selector: 'app-column-header',
  templateUrl: './column-header.component.html',
  styleUrls: ['./column-header.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ColumnHeaderComponent implements OnInit, OnChanges, OnDestroy {
  @Input()
  status: string;
  column: Column = Column.NULL;
  storyPoints: Subject<string> = new Subject<string>();
  showLeaderBoard = false;

  private column$: Subscription = new Subscription();

  constructor(private columnsService: ColumnsService) {
  }

  ngOnInit() {
  }

  moveLeft() {
    this.columnsService.moveLeft(this.status);
  }

  moveRight() {
    this.columnsService.moveRight(this.status);
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.status) {
      // console.log(changes.status.firstChange, changes.status.previousValue, changes.status.currentValue);
      if (changes.status.previousValue) {
        this.column$.unsubscribe();
      }
      if (this.status.length > 0) {
        this.column$ = this.columnsService.changes(this.status).subscribe(
          column => {
            this.column = column;
            this.storyPoints.next(String(column.storyPoints));
          }
        );
      }
    }
  }

  ngOnDestroy(): void {
    this.column$.unsubscribe();
  }

  remove() {
    this.columnsService.remove(this.status);
  }

  toggleLeaderBoard() {
    this.showLeaderBoard = !this.showLeaderBoard;
  }
}
