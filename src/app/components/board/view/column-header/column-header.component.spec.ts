import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ColumnHeaderComponent} from './column-header.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {ColumnsService} from '../columns.service';
import {Subject} from 'rxjs';
import {Column} from '../Column';

describe('ColumnHeaderComponent', () => {
  let component: ColumnHeaderComponent;
  let fixture: ComponentFixture<ColumnHeaderComponent>;
  let moveLeftSpy;
  let moveRightSpy;
  let columnChangesSpy;
  let columnChanges$;

  beforeEach(async(() => {
    moveLeftSpy = jasmine.createSpy('moveLeft');
    moveRightSpy = jasmine.createSpy('moveRight');
    columnChanges$ = new Subject<Column>();
    columnChangesSpy = jasmine.createSpy('change').and.returnValue(
      columnChanges$.asObservable());
    TestBed.configureTestingModule({
      declarations: [ColumnHeaderComponent],
      providers: [{
        provide: ColumnsService,
        useValue: {}
      }],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ColumnHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  // TODO: left, right, remove, toggleLeaderBoard click
});
