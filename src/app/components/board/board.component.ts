import {
    ChangeDetectionStrategy,
    ChangeDetectorRef,
    Component,
    OnDestroy,
    OnInit,
} from '@angular/core';
import { EMPTY, interval, Observable, Subscription, timer } from 'rxjs';
import { UserSelectService } from '../../services/user-select.service';
import { debounce, switchMap, tap, withLatestFrom } from 'rxjs/operators';
import { Ticket } from '../../models/Ticket';
import { EntityManagerService } from '../../data-access-layer/entity-manager.service';
import { ColumnsService } from './view/columns.service';
import { EntityLocalStorageService } from '../../data-access-layer/entity-local-storage/entity-local-storage.service';

@Component({
    selector: 'app-board',
    templateUrl: './board.component.html',
    styleUrls: ['./board.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BoardComponent implements OnInit, OnDestroy {
    sub: Subscription = new Subscription();
    ticketCount = 0;
    tickets: Ticket[];

    selectorKey = 'boardJql';

    private jql$: Observable<string>;
    private tickets$: Observable<Ticket[]>;
    private isRefresh = false;
    private lastJQL = '';

    constructor(
        private cd: ChangeDetectorRef,
        private storage: UserSelectService,
        private columnsService: ColumnsService,
        private em: EntityManagerService,
        private jqlStorage: EntityLocalStorageService,
    ) {}

    ngOnInit() {
        this.jql$ = this.storage.userSelected(this.selectorKey);

        this.sub.add(
            interval(5 * 60 * 1000)
                .pipe(withLatestFrom(this.jql$))
                .subscribe(([_, jql]) => {
                    this.storage.userSet(this.selectorKey, jql, false);
                }),
        );

        let firstView = true;
        this.tickets$ = this.jql$.pipe(
            debounce(() => (firstView ? EMPTY : timer(2000))),
            tap(() => (firstView = false)),
            tap((jql: string) => {
                this.isRefresh = jql === this.lastJQL;
                this.lastJQL = jql;
            }),
            tap(() => this.columnsService.resetColumns()),
            switchMap(jql => this.em.findTicketsByJQL(jql)),
        );

        this.sub.add(
            this.tickets$
                .pipe(
                    tap(tickets => (this.ticketCount = tickets.length)),
                    tap(tickets => this.collectColumns(tickets)),
                )
                .subscribe(tickets => {
                    if (this.isRefresh) {
                        this.updateTickets(tickets);
                    } else {
                        this.tickets = tickets;
                        if (tickets.length) {
                            this.jqlStorage.save(this.selectorKey, this.lastJQL);
                        }
                    }

                    this.cd.detectChanges();
                }),
        );
    }

    ngOnDestroy(): void {
        this.sub.unsubscribe();
    }

    private collectColumns(tickets: Ticket[]) {
        tickets.forEach(t => {
            this.columnsService.update(t);
            this.columnsService.append(t.status);
        });
    }

    private updateTickets(tickets: Ticket[]) {
        tickets.forEach(ticket => {
            const ix = this.tickets.findIndex(t => t.key === ticket.key);
            if (ix < 0) {
                this.tickets.push(ticket);
            } else {
                this.tickets.splice(ix, 1, ticket);
            }
        });
        this.tickets = [...this.tickets];
    }
}
