import {ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import {EntityManagerService} from '../../data-access-layer/entity-manager.service';
import {Subscription} from 'rxjs';
import {Project} from '../../models/Project';

@Component({
  selector: 'app-projects-view',
  templateUrl: './projects-view.component.html',
  styleUrls: ['./projects-view.component.scss']
})
export class ProjectsViewComponent implements OnInit, OnDestroy {
  projects: Project[];

  private sub: Subscription;

  constructor(private em: EntityManagerService, private cd: ChangeDetectorRef) {
  }

  ngOnInit() {
    this.sub = this.em.fetchProjects().subscribe(p => {
      this.projects = p;
      this.cd.detectChanges();
    });
  }

  projectIdFn(index, item: Project) {
    return item.id;
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }
}
