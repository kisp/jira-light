import {Component, OnDestroy, OnInit} from '@angular/core';
import {EntityManagerService} from '../../data-access-layer/entity-manager.service';
import {UserSelectService} from '../../services/user-select.service';
import {Dashboard} from '../../models/Dashboard';
import {Subscription} from 'rxjs';
import {Project} from '../../models/Project';

@Component({
  selector: 'app-dashboards-view',
  templateUrl: './dashboards-view.component.html',
  styleUrls: ['./dashboards-view.component.scss']
})
export class DashboardsViewComponent implements OnInit, OnDestroy {
  dashboards: Dashboard[];

  private subscription: Subscription;

  constructor(private em: EntityManagerService, private db: UserSelectService) {
  }

  ngOnInit() {
    this.subscription = this.em.fetchAllGroupsDashboards().subscribe(dashboards => this.dashboards = dashboards);
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  dashboardIdFn(index, item: Dashboard) {
    return item.id;
  }

}
