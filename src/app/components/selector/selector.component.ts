import {Component, Input, OnInit} from '@angular/core';
import {UserSelectService} from '../../services/user-select.service';

@Component({
  selector: 'app-selector',
  templateUrl: './selector.component.html',
  styleUrls: ['./selector.component.scss']
})
export class SelectorComponent implements OnInit {

  @Input()
  key: string;
  @Input()
  value: string;

  constructor(private db: UserSelectService) {
  }

  ngOnInit() {
  }

  clicked($event: MouseEvent) {
    this.db.userSet(this.key, this.value);
  }
}
