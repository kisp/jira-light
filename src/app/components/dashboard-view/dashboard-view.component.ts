import {Component, OnDestroy, OnInit} from '@angular/core';
import {EntityManagerService} from '../../data-access-layer/entity-manager.service';
import {UserSelectService} from '../../services/user-select.service';
import {switchMap} from 'rxjs/operators';
import {Dashboard} from '../../models/Dashboard';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-dashboard-view',
  templateUrl: './dashboard-view.component.html',
  styleUrls: ['./dashboard-view.component.scss']
})
export class DashboardViewComponent implements OnInit, OnDestroy {

  dashboard: Dashboard;

  private subscription: Subscription;

  constructor(
    private db: UserSelectService,
    private em: EntityManagerService) {
    this.subscription = new Subscription();
  }

  ngOnInit() {
    this.subscription.add(
      this.db.userSelected('dashboard')
        .pipe(
          switchMap(dashboardId => this.em.fetchDashboardById(Number(dashboardId)))
        )
        .subscribe(d => this.dashboard = d));
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

}
