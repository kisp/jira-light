import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {FormsModule} from '@angular/forms';
import {MenuModule} from 'primeng/menu';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {AuthCredentialsComponent} from './components/auth-credentials/auth-credentials.component';
import {TicketViewComponent} from './components/ticket-view/ticket-view.component';
import {HttpClientModule} from '@angular/common/http';
import {FiltersViewComponent} from './components/filters-view/filters-view.component';
import {DashboardsViewComponent} from './components/dashboards-view/dashboards-view.component';
import {ProjectsViewComponent} from './components/projects-view/projects-view.component';
import {ProjectComponent} from './components/projects-view/project/project.component';
import {SelectorComponent} from './components/selector/selector.component';
import {DashboardComponent} from './components/dashboards-view/dashboard/dashboard.component';
import {DashboardViewComponent} from './components/dashboard-view/dashboard-view.component';
import {MenubarModule} from 'primeng/menubar';
import {CardModule} from 'primeng/card';
import {ButtonModule} from 'primeng/button';
import {FieldComponent} from './components/ticket-view/field/field.component';
import {TooltipModule} from 'primeng/tooltip';
import {ToastModule} from 'primeng/toast';
import {MessageService} from 'primeng/api';
import {InputTextModule} from 'primeng/inputtext';
import {DialogModule} from 'primeng/dialog';
import {ShowDomChangesDirective} from './show-dom-changes.directive';

@NgModule({
  declarations: [
    AppComponent,
    AuthCredentialsComponent,
    TicketViewComponent,
    FiltersViewComponent,
    DashboardsViewComponent,
    ProjectsViewComponent,
    ProjectComponent,
    SelectorComponent,
    DashboardComponent,
    DashboardViewComponent,
    FieldComponent,
    ShowDomChangesDirective
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    FormsModule,
    MenuModule,
    HttpClientModule,
    MenubarModule,
    CardModule,
    ButtonModule,
    TooltipModule,
    ToastModule,
    InputTextModule,
    DialogModule
  ],
  providers: [MessageService],
  exports: [
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
