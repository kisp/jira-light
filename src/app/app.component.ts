import {Component, OnInit} from '@angular/core';
import {MenuItem} from 'primeng/api';
import {Observable} from 'rxjs';
import {RoutingService} from './services/routing.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  menuItems: Observable<MenuItem[]>;

  constructor(private routing: RoutingService) {
  }

  ngOnInit() {
    this.menuItems = this.routing.getMenuItemsWithSelection();
  }
}
