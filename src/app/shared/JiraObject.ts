import {wrap} from './utils/deepProperty';

export class JiraObject {
  protected source: any;

  constructor(o: any) {
    this.source = wrap(o || {});
  }
}
