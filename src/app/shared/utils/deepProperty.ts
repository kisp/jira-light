const endlessProxyHandler = {
  get: (target, path) => {
    if (path === 'value') {
      return target.value;
    } else if (path === 'array') {
      return target.value || [];
    }

    let newTarget = {};
    if (typeof target[path] === 'object' && target[path] !== null) {
      newTarget = target[path];
    }

    const proxy = new Proxy(newTarget, endlessProxyHandler);
    proxy.value = target[path];
    return proxy;
  }
};

export const wrap = (o) => new Proxy(o, endlessProxyHandler);
export const unwrap = (o) => o.value;
export const asString = (o) => typeof (o.value) !== 'undefined' ? o.value : '';
export const asArray = (o) => Array.isArray(o.value) ? o.value : [];
