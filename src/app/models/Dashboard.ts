import {JiraObject} from '../shared/JiraObject';
import {Person} from './Person';

export class Dashboard extends JiraObject {
  owner: Person;

  constructor(o) {
    super(o);
    this.owner = new Person(this.source.owner.value);
  }

  get id() {
    return this.source.id.value;
  }

  get name() {
    return this.source.name.value;
  }

  get viewUrl() {
    return this.source.view.value || '#';
  }

  toString() {
    return this.name;
  }
}
