import {JiraObject} from '../shared/JiraObject';

export class Group extends JiraObject {
  constructor(o) {
    super(o);
  }

  get name() {
    return this.source.name.value;
  }

  toString() {
    return this.name;
  }
}
