import {JiraObject} from '../shared/JiraObject';

export class TicketStatus extends JiraObject {
  constructor(o) {
    super(o);
  }
  get name(): string {
    return this.source.name.value;
  }
  get description(): string {
    return this.source.description.value;
  }
  get color(): string {
    return this.source.statusCategory.colorName.value;
  }

  get icon(): string {
    return this.source.iconUrl.value;
  }
}
