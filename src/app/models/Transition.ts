import {JiraObject} from '../shared/JiraObject';

export class Transition extends JiraObject {
  constructor(o) {
    super(o);
  }

  get id(): number {
    return this.source.id.value;
  }

  get name(): string {
    return this.source.name.value;
  }

  get icon(): string {
    return this.source.to.iconUrl.value;
  }

  get color(): string {
    return this.source.to.statusCategory.colorName.value;
  }

  toString() {
    return this.name;
  }
}
