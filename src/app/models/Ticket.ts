import { JiraObject } from '../shared/JiraObject';
import { Person } from './Person';
import { TicketDescription } from './TicketDescription';
import { TicketStatus } from './TicketStatus';
import { CustomField } from './CustomField';
import { TicketLink } from './TicketLink';

export class Ticket extends JiraObject {
  static NULL: Ticket = new Ticket({});

  reporter: Person;
  assignee: Person;

  subTasks: Ticket[];
  description: TicketDescription;
  status: TicketStatus;

  loaded: boolean;

  fieldDefinitions: CustomField[];

  constructor(o: any) {
    super(o);
    this.reporter = new Person(this.source.fields.reporter.value);
    this.assignee = new Person(this.source.fields.assignee.value);
    this.subTasks = this.source.fields.subtasks.array.map(t => new Ticket(t));
    this.description = new TicketDescription(this.source.fields.description.value);
    this.status = new TicketStatus(this.source.fields.status.value);
    if (this.source.fields.value) {
      this.loaded = true;
    }
  }

  get key(): string {
    return this.source.key.value;
  }

  get title(): string {
    return this.source.fields.summary.value;
  }

  get components(): string[] {
    return this.source.fields.components.array.map(c => c.name);
  }

  get type(): string {
    return this.source.fields.issuetype.name.value;
  }

  get icon(): string {
    return this.source.fields.issuetype.iconUrl.value;
  }

  get priority(): string {
    return this.source.fields.priority.name.value;
  }

  get priorityIcon(): string {
    return this.source.fields.priority.iconUrl.value;
  }

  get created(): Date {
    return new Date(this.source.fields.created.value);
  }

  get updated(): Date {
    return new Date(this.source.fields.updated.value);
  }

  get outwardKeys(): string[] {
    return this.source.fields.issuelinks.array
      .filter(i => !!i.outwardIssue)
      .map(i => i.outwardIssue.key);
  }

  get linkedTickets(): TicketLink[] {
    return this.source.fields.issuelinks.array
      .map(i => !!i.outwardIssue ? i.outwardIssue : !!i.inwardIssue && i.inwardIssue)
      .map(o => new TicketLink(o));
  }

  get fields() {
    return this.source.fields.value;
  }

  get storyPoints(): number {
    const arr = this.fetchCustomFields('Story Points');
    return arr.filter(v => Number.isFinite(v))[0];
  }

  toString(): string {
    return `${ this.key } ${ this.title }`;
  }

  /**
   * Customfields are represented by field.id :(
   * This method searches customfield by text fieldName
   * @returns all the fieldvalues with the exact name
   * @param fieldName: string
   */
  fetchCustomFields(fieldName: string): Array<any> {
    return this.fieldDefinitions
      .filter(fieldDef => fieldDef.name === fieldName)
      .map(fieldDef => this.source.fields.value[fieldDef.id]);
  }
}

