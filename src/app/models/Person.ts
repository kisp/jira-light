import {JiraObject} from '../shared/JiraObject';

export class Person extends JiraObject {
  constructor(o) {
    super(o);
  }
  get id() {
    return this.source.accountId.value;
  }
  get name() {
    return this.source.displayName.value;
  }
  get timeZone() {
    return this.source.timeZone.value;
  }
  get key() {
    return this.source.key.value;
  }
  get icon() {
    return this.source.avatarUrls['48x48'].value;
  }

  toString() {
    return this.name;
  }
}
