import {JiraObject} from '../shared/JiraObject';

export class Project extends JiraObject {
  constructor(o) {
    super(o);
  }
  get id() {
    return this.source.id.value;
  }
  get name() {
    return this.source.name.value;
  }
  get key() {
    return this.source.key.value;
  }
  get icon() {
    return this.source.avatarUrls['48x48'].value;
  }

  toString() {
    return `${this.key} - ${this.name}`;
  }
}
