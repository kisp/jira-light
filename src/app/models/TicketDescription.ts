import {JiraObject} from '../shared/JiraObject';

const collectText = (input: Array<any>) => {
  const collect = [];

  input.forEach(o => {
    if (Array.isArray(o.content)) {
      collect.push(collectText(o.content));
    }
    if (typeof o.text === 'string') {
      collect.push(o.text);
    }
  });

  return collect.join('\n');
};

export class TicketDescription extends JiraObject {
  constructor(o) {
    super(o);
  }

  get text() {
    return collectText(this.source.content.array);
  }
}
