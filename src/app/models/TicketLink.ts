import { JiraObject } from '../shared/JiraObject';

export class TicketLink extends JiraObject {
  constructor(o) {
    super(o);
  }

  get key(): string {
    return this.source.key.value;
  }

  toString() {
    return this.key;
  }
}
