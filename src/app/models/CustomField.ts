import {JiraObject} from '../shared/JiraObject';

/**
 * <pre>
 * {
 *  "id": "description",
 *   "name": "Description",
 *  "custom": false,
 *   "orderable": true,
 *   "navigable": true,
 *   "searchable": true,
 *   "clauseNames": [
 *     "description"
 *   ],
 *   "schema": {
 *     "type": "string",
 *     "system": "description"
 *   }
 * }
 * </pre>
 */
export class CustomField extends JiraObject {

  constructor(o: any) {
    super(o);
  }

  get id(): string {
    return this.source.id.value;
  }

  get name(): string {
    return this.source.name.value;
  }
}
