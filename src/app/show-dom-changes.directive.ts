import {Directive, ElementRef, OnDestroy, OnInit, Renderer2} from '@angular/core';

@Directive({
  selector: '[appShowDomChanges]'
})
export class ShowDomChangesDirective implements OnInit, OnDestroy {

  private changes: MutationObserver;

  private removeClassTimer;

  constructor(private elementRef: ElementRef, private renderer: Renderer2) {
  }

  ngOnInit(): void {
    this.changes = new MutationObserver((mutations => {
      mutations.forEach((mutation: MutationRecord) => this.addHighLight(mutation));
    }));

    this.changes.observe(this.elementRef.nativeElement, {
      subtree: true,
      childList: true
    });
  }

  ngOnDestroy(): void {
    this.changes.disconnect();
    if (this.removeClassTimer) {
      this.removeClassTimer();
    }
  }

  addHighLight(mutation: MutationRecord) {
    if (!this.removeClassTimer) {
      const node: Node = mutation.target;
      // tslint:disable-next-line:prefer-for-of
      for (let i = 0; i < node.parentElement.childNodes.length; i++) {
        if (node.isSameNode(node.parentElement.childNodes[i])) {
          this.putHighlight(node.parentElement.childNodes, i);
          break;
        }
      }
    }
  }

  putHighlight(parents: NodeList, id: number ) {
    const el = parents[id];
    if (el) {
      this.renderer.addClass(el, 'show-highlighted');
      this.removeClassTimer = setTimeout(this.removeHighLight.bind(this, el), 500);
    }
  }

  removeHighLight(el: HTMLElement) {
    this.removeClassTimer = null;
    this.renderer.removeClass(el, 'show-highlighted');
  }
}
