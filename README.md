# Jira-light

The main purpose of this application is learning angular.


## Jira api links

[Documentation can be found here...](https://developer.atlassian.com/cloud/jira/platform/rest/v3/?utm_source=%2Fcloud%2Fjira%2Fplatform%2Frest%2F&utm_medium=302#api-rest-api-3-issue-issueIdOrKey-get)

Example request:

```
curl --request GET --url 'https://xeranet.atlassian.net/rest/api/3/issue/$JIRA_TICKET' --user "$JIRA_USER_EMAIL:$JIRA_TOKEN" --header 'Accept: application/json' --header 'Content-Type: application/json' 
```

## Features

```
- TODO
+ DONE
~ partially DONE
? need specification
```

#### Jira Objects
```
+ Basic service architecture
+ Entity Manager + Http services
+ projects
+ filters
+ boards
+ myself
+ execute JQL
+ Retreive multiple pages of data
+ Retreive 'story points' -> found in custom field
- Update ticket 'Story Points'
```

#### Column
```
+ Story point aggregation on board header
- Remove columns - close button
- Check hiding columns - is it a good design? is it worth of it to put it on a checkbox?
- Ranking people on column-headers
```
#### Board
```
- JQL history
- Management of JQLs
```
